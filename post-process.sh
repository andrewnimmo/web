#!/bin/sh
set -eu
URL="http://localhost:8888"

nginx -g 'daemon off;' -c $PWD/nginx.conf &
PID=$!

echo -n "Waiting for nginx"
while ! curl -f "$URL"; do echo -n .; sleep 1; done
echo
node prerender/prerender.js "$URL"

kill "$PID"
wait "$PID"
cd dist/
