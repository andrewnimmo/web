const errorMessages = {
  'Failed to fetch': 'Service not responding, please try again later.',
};

export function errorMessage(err) {
  const msg = errorMessages[err];
  if (msg == undefined) {
    return err;
  }
  return msg;
};
