import React from 'react';
import PropTypes from 'prop-types';
import {
    Link,
    browserHistory,
} from 'react-router';

import Plans from './plans';

// Users
// require since import can't pass arguments
const DemoImage =
  require('../images/latency-grafana.png?sizes[]=128,sizes[]=256,sizes[]=512');
const LogoSoundCloud =
  require('../images/logo-soundcloud.png?sizes[]=128,sizes[]=256,sizes[]=512');
const LogoMatrix =
  require('../images/logo-matrix.png?sizes[]=128,sizes[]=256,sizes[]=512');
const LogoKoko =
  require('../images/logo-koko.png?sizes[]=128,sizes[]=256,sizes[]=512');

// Monitoring Systems
const LogoPrometheus =
  require('../images/logo-prometheus.png?sizes[]=128,sizes[]=256,sizes[]=512');
const LogoDatadog =
  require('../images/logo-datadog.png?sizes[]=128,sizes[]=256,sizes[]=512');
const LogoInfluxdata =
  require('../images/logo-influxdata.png?sizes[]=128,sizes[]=256,sizes[]=512');
const LogoSensu =
  require('../images/logo-sensu.png?sizes[]=128,sizes[]=256,sizes[]=512');
const LogoZabbix =
  require('../images/logo-zabbix.png?sizes[]=128,sizes[]=256,sizes[]=512');
const LogoCollectd =
  require('../images/logo-collectd.png?sizes[]=128,sizes[]=256,sizes[]=512');
const LogoNagios =
  require('../images/logo-nagios.png?sizes[]=128,sizes[]=256,sizes[]=512');
// const LogoLibrato =
//  require('../images/logo-librato.png?sizes[]=128,sizes[]=256,sizes[]=512');
const LogoGraphite =
  require('../images/logo-graphite.png?sizes[]=128,sizes[]=256,sizes[]=512');
// const LogoSysdig =
//  require('../images/logo-sysdig.png?sizes[]=128,sizes[]=256,sizes[]=512');


export default class Home extends React.Component {
  render() {
    return (
    <div id="home">
      <article className="intro">
        <div className="wrapper wrapper-wide">
          <h1>Performance and Availability Metrics For Your Monitoring System.
          </h1>
        </div>
        <div className="wrapper">
          <p className="text-large">
            Latency.at measures performance and availability of your sites and
            services from multiple global locations and provides the results to
            your Monitor System.
            <br />
          </p>
          <div className="button-bar">
            <button className="mobile-block" onClick={() => {
              browserHistory.push('/login');
            }}>
              Login
            </button>
            <button className="button-primary mobile-block"
              onClick={() => {
                browserHistory.push('/signup');
              }}>
              Sign up
            </button>
          </div>
        </div>
      </article>

      <article>
        <div className="wrapper">
          <h3>Open API for your Monitoring System</h3>
          <p>Latency.at operates a growing network of distributed probes
          providing performance and availability metrics in the widely
          supported&nbsp;
          <a href="https://prometheus.io/docs/instrumenting/exposition_formats/">Prometheus
          Exposition Format</a>. Have all your metrics in your monitoring
          system. No need to configure and check your services on an external
          website!
          </p>
        </div>
        <hr />
        <div className="wrapper">
          <h3>Easy configuration</h3>
          <p>No need to manage external configuration. Setting up targets,
          resolution and probes all happens in your monitoring system config. No
          matter whether you run on Kubernetes, GCE, AWS or your private cloud.
          See how to integrate with your existing monitoring system:</p>
          <div className="systems">
            <Link to="/docs/prometheus"><img srcSet={LogoPrometheus.srcSet}
              src={LogoPrometheus.src} alt="Prometheus Logo"/></Link>

            <Link to="/docs/datadog"><img srcSet={LogoDatadog.srcSet}
              src={LogoDatadog.src} alt="Datadog Logo"/></Link>

            <Link to="/docs/influxdata"><img srcSet={LogoInfluxdata.srcSet}
              src={LogoInfluxdata.src} alt="Influxdata Logo"/></Link>

            <Link to="/docs/sensu"><img srcSet={LogoSensu.srcSet}
              src={LogoSensu.src} alt="Sensu Logo"/></Link>

            <Link to="/docs/zabbix"><img srcSet={LogoZabbix.srcSet}
              src={LogoZabbix.src} alt="Zabbix Logo"/></Link>

            <Link to="/docs/collectd"><img srcSet={LogoCollectd.srcSet}
              src={LogoCollectd.src} alt="Collectd Logo"/></Link>

            <Link to="/docs/nagios"><img srcSet={LogoNagios.srcSet}
              src={LogoNagios.src} alt="Nagios Logo"/></Link>

            <Link to="/docs/graphite"><img srcSet={LogoGraphite.srcSet}
              src={LogoGraphite.src} alt="Graphite Logo"/></Link>
          </div>
        </div>
        <hr />
        <div className="wrapper">
          <h3>Flexible and Correct</h3>
          <p>Our probes can provide metrics for any target reachable over
          the public internet. Not only can it be used to monitor the speed of
          your websites or APIs but also remote services you might depend
          on or just be interested in their performance.
          On each request, the probes send out request to the configured target
          and return fresh, unaggregated metrics. This allows you to aggregate,
          filter and correlate them metrics with your existing metrics.</p>
        </div>
        <hr />
        <div className="wrapper">
          <h3>Prometheus/Grafana Demo</h3>
          <div style={{position: 'relative'}}>
            <button className="button-primary"
              onClick={() => window.open('https://demo.latency.at', '_blank')}
              style={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                width: '16em',
                height: '4em',
                marginLeft: '-8em',
                marginTop: '-2em',
              }}>
              View Demo</button>
            <img
              style={{width: '100%'}}
              srcSet={DemoImage.srcSet}
              src={DemoImage.src}
              alt="Grafana Dashboard showing latency.at metrics"/>
          </div>
        </div>
        <hr />
        <div className="wrapper">
          <h3>Multiple protocols</h3>
          <p>The probes run a modified version of the open source Prometheus <a
          href="https://github.com/prometheus/blackbox_exporter">Blackbox
          Exporter</a> to provide metrics. It supports HTTP, HTTPS, DNS, TCP
          and ICMP probes. See the <Link to="/docs">documentation</Link> on all
          available checks and how to configure them. Support for custom GET
          parameters for HTTP and HTTPS checks. POST parameters and custom DNS
          queries coming soon!</p>
        </div>
        <hr />
        <div className="wrapper">
          <h3>Customer Driven</h3>
          <p>While we built latency.at with years of experience operating
          and monitoring sites with millions of users around the globe, we
          believe the best way to solve your problems is by listening to
          you.
          Having trouble integrating with your monitoring system? Need to
          monitor an unsupported protocol? Or prefer post-paid model? <a
          href="mailto:sales@latency.at">Talk to us!</a></p>
        </div>
        <hr />
        <div className="wrapper">
          <h3>Free Tier, Affordable Plans</h3>
          <p>We have one of the most affordable pricing in the SaaS monitoring
          industry.
          You need high resolution or want to monitor many targets? In our
          Advanced Plan you get 15 Million requests for $100, or 150.000
          requests/$.
          To get you started or help you monitor your side projects, we offer a
          completely free plan for with 50k requests.</p>
        </div>
      </article>
      <article>
        <div style={{textAlign: 'center', margin: '3em 0'}}>
          <h3><span className="mobile-br">Plans for</span> every need</h3>
          <Plans api={this.props.api} hideInfo={true} onClick={() =>
            browserHistory.push('/dashboard/plans')} />
          <p style={{marginBottom: '1em'}}>
            <b>Get started with our free Plan</b>
          </p>
          <p className="plan-home">
            Subscribe to a plan to get monthly request credits.
            The free Plan includes 50,000 Requests (~1.14 req/min).
            <br />
            Need something custom?&nbsp;
            <a href="mailto:sales@latency.at">Talk to us</a>.&nbsp;
            For EU individuals, plus regional VAT rate.
          </p>
        </div>
      </article>
      <article>
        <div className="wrapper wrapper-wide" style={{textAlign: 'center'}}>
          <h3>Some of our Customers</h3>
          <div className="logos">
            <a href="https://soundcloud.com/"><img srcSet={LogoSoundCloud.srcSet} src={LogoSoundCloud.src} alt="SoundCloud Logo"/></a>
            <a href="https://itskoko.com/"><img srcSet={LogoKoko.srcSet} src={LogoKoko.src} alt="Koko Logo"/></a>
            <a href="https://matrix.org/"><img srcSet={LogoMatrix.srcSet} src={LogoMatrix.src} alt="Matrix Logo"/></a>
          </div>
        </div>
      </article>
    </div>);
  }
};

Home.propTypes = {
  modalShow: PropTypes.func,
  api: PropTypes.object,
  user: PropTypes.object,
};
