import React from 'react';

const Demo = (props) => {
  return <div>
    <p className="wrapper">
      <h1>Demo</h1>
      Interactive online demo coming soon.<br />
      For now, this is a Grafana dashboard powered by Latency.at:
      <br />
    </p>
    <iframe style={{display: 'block', width: '100%', height: '1600px'}}
      src="https://snapshot.raintank.io/dashboard/snapshot/FhkHHzkQnNn2joe3p4aMsPKOZcdD1D4I" />
  </div>;
};

export default Demo;
