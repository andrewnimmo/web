import React from 'react';
import PropTypes from 'prop-types';

const Alert = (props) => {
  const className = props.slidein ? 'alert-slidein' : 'alert';
  return <div
    style={props.style}
    className={className + ' alert-' + props.status || 'unknown'}>
    {props.children}</div>;
};

export default Alert;
Alert.propTypes = {
  children: PropTypes.element.isRequired,
  style: PropTypes.object,
  status: PropTypes.string,
  slidein: PropTypes.bool,
};
