import React from 'react';
import PropTypes from 'prop-types';
import scrollToComponent from 'react-scroll-to-component';
import Scrollspy from 'react-scrollspy';

import QuickStart from './prometheus/quickstart.md';
import Config from './prometheus/config.md';
import ConfigGenerator from './prometheus/generator';

import defaultPlans from '../data/plans';

export default class Prometheus extends React.Component {
  constructor(props) {
    super(props);
    this.scrollTo = this.scrollTo.bind(this);
    this.setRefs = this.setRefs.bind(this);
    this.state = {
      plans: defaultPlans,
    };
  }
  componentWillMount() {
    this.props.api.Plans.get()
    .then((plans) => {
      this.setState({
        plans: Object.values(plans),
      });
    })
    .catch((error) => this.props.notify(error));
    if (this.props.api.LoggedIn) {
      this.props.api.Tokens.get()
      .then((tokens) => {
        this.setState({token: tokens.tokens[0] && tokens.tokens[0].token});
      })
      .catch((error) => {
        if (this.props.loadFailed) {
          this.props.loadFailed(error);
          return;
        };
        // Ignore error when not on dashboard
      });
    }
  }
  setRefs(refs) {
    this.setState({genRefs: refs});
  }
  scrollTo(e) {
    const name = e.target.dataset.ref;
    const ref = this.refs[name] || this.state.genRefs[name];

    scrollToComponent(ref, {
      align: 'top',
    });
  }
  render() {
    return <article id="configuration-view">
      <div className="wrapper-wide"><h1>Prometheus</h1></div>
      <nav ref="nav">
        <Scrollspy items={['quickstart', 'configuration', 'config-generator',
          'satellites', 'download']}
          currentClassName="active" onEvent={this.onScroll}>
          <li className="active">
            <a href="#" onClick={this.scrollTo} data-ref="quickstart">
              Quick Start</a></li>
          <li><a href="#" onClick={this.scrollTo} data-ref="configuration">
            Configuration</a></li>
          <li><a href="#" onClick={this.scrollTo} data-ref="generator">
            Generator</a></li>
          <li><a href="#" onClick={this.scrollTo} data-ref="satellites">
            Probes</a></li>
          <li><a href="#" onClick={this.scrollTo} data-ref="download">
            Download</a></li>
        </Scrollspy>
      </nav>
      <div>
        <article>
          <div id="quickstart" ref="quickstart" className="markdown"
            dangerouslySetInnerHTML={{__html: QuickStart}} />
          <div id="configuration" ref="configuration" className="markdown"
            dangerouslySetInnerHTML={{__html: Config}} />
        </article>
        <article ref="generator">
        <ConfigGenerator
          notify={this.props.notify}
          setRefs={this.setRefs}
          plans={this.state.plans}
          token={this.state.token}/>
        </article>
      </div>
    </article>;
  };
};

Prometheus.propTypes = {
  // Required but leading to false positives due to usage of cloneElement.
  api: PropTypes.object,
  notify: PropTypes.func,
  loadFailed: PropTypes.func,
};
