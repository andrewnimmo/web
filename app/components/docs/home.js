import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router';

import Reference from './reference.md';

const Docs = (props) => {
  return <article>
        <div className="wrapper-wide">
          <h1>Documentation</h1>
        </div>
        <h2>Integration Guides</h2>
        <nav className="wrapper-wide" style={{marginBottom: '2em'}}>
          <ul>
            <li><Link to='/docs/prometheus'>Prometheus</Link></li>
            <li><Link to='/docs/datadog'>Datadog</Link></li>
            <li><Link to='/docs/influxdata'>Influxdata</Link></li>
            <li><Link to='/docs/sensu'>Sensu</Link></li>
            <li><Link to='/docs/zabbix'>Zabbix</Link></li>
            <li><Link to='/docs/nagios'>Nagios</Link></li>

            <li><Link to='/docs/collectd'>Collectd</Link></li>
            <li><Link to='/docs/graphite'>Graphite</Link></li>
          </ul>
        </nav>
        <h2>Reference</h2>
        <div className="wrapper markdown"
          dangerouslySetInnerHTML={{__html: Reference}}>
        </div>
      </article>;
};

export default Docs;
Docs.propTypes = {
  api: PropTypes.object,
  notify: PropTypes.func,
};
