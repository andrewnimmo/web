InfluxData's Kapacitor [supports Prometheus
metrics](https://docs.influxdata.com/kapacitor/v1.3/pull_metrics/scraping-and-discovery/)
out the box. All you need is to configure Kapacitor to scrape Latency.at with
your token:

```
[[static-discovery]]
  enabled = true
  id = "lat-sfo1"
  targets = ["https://sfo1.do.mon.latency.at/probe?target=https://latency.at"]
  bearer-token = "your token"
  [static.labels]
    region = "sfo1.do"
[[static-discovery]]
  enabled = true
  id = "lat-nyc1"
  targets = ["https://nyc1.do.mon.latency.at/probe?target=https://latency.at"]
  bearer-token = "your token"
  [static.labels]
    region = "nyc1.do"
[[static-discovery]]
  enabled = true
  id = "lat-fra1"
  targets = ["https://fra1.do.mon.latency.at/probe?target=https://latency.at"]
  bearer-token = "your token"
  [static.labels]
    region = "fra1.do"
```
