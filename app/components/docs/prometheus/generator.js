import React from 'react';
import PropTypes from 'prop-types';

import ClipboardButton from 'react-clipboard.js';
import TextareaAutosize from 'react-autosize-textarea';
import CountTo from 'react-count-to';

import FileSaver from 'file-saver';

const satellites = [
  ['do', 'DigitalOcean', [
    ['sfo1', 'us-west: San Francisco'],
    ['nyc1', 'us-east: New York'],
    ['fra1', 'eu-central: Frankfurt'],
    ['ams2', 'eu-west: Amsterdam'],
    ['blr1', 'ap-east: Bangalore'],
    ['sgp1', 'ap-central: Singapore'],
  ],
]];

const modules = [
  [
    'http_2xx', 'Sends GET, expects 2xx', 'http://',
  ],
  [
    'http_post_2xx', 'Sends POST, expects 2xx', 'http://',
  ],
  [
    'tcp_connect', 'Connects via TCP, expects established connection',
    'hal9000.example.com',
  ],
  [
    'pop3s_banner', 'Connects via TCP+TLS, expects "^+OK" response',
    'smtp.gmail.com',
  ],
  [
    'ssh_banner', 'Connects via TCP, expects "^SSH-2.0- response',
    'wopr.norad.mil',
  ],
  [
    'icmp', 'Sends ICMP echo request (ping), expects response',
    'skynet.example',
  ],
];

const relableConfig = `    relabel_configs: &relabel_config
      - source_labels: [__address__]
        regex: (.*)@(.*)(:80)?
        target_label: __param_target
        replacement: \${1}
      - source_labels: [__param_target]
        regex: (.*)
        target_label: instance
        replacement: \${1}
      - source_labels: [__address__]
        regex: .*@([^.]*).*
        target_label: region
        replacement: \${1}
      - source_labels: [__address__]
        regex: .*@[^.]*.(.*)
        target_label: provider
        replacement: \${1}
      - source_labels: [region,provider]
        separator: .
        regex: (.*)
        target_label: __address__
        replacement: \${1}.mon.latency.at
      - source_labels: []
        target_label: __scheme__
        replacement: https
`;

function genJob(token, module, targets, satellites) {
  let moduleName = module[0];
  let moduleDesc = module[1];
  let targetCount = 0;
  let config = `  ### ${moduleDesc} ###
  - job_name: 'latency-at-${moduleName}'
    bearer_token: '${token}'
    metrics_path: /probe
    params:
      module: [${moduleName}]
    static_configs:
      - targets:
`;
  for (let target of targets.split('\n')) {
    for (let sat of Object.keys(satellites)) {
      if (!satellites[sat]) {
        continue;
      }
      config += `        - ${target}@${sat}\n`;
      targetCount++;
    }
  }
  return [config, targetCount];
}

function genConfig(token, state) {
  if (!token) {
    token = 'your-auth-token';
  }
  if (!state.enabledSatellites) {
    return;
  }
  let config = `global:
  scrape_interval: ${state.scrapeInterval}s

scrape_configs:
`;
  let targetCount = 0;
  let first = true;
  for (let module of modules) {
    if (!state[module[0]]) {
      continue;
    }
    let job = genJob(token, module, state[module[0]], state.enabledSatellites);
    config += job[0];
    targetCount += job[1];
    if (first) {
      first = false;
      config += relableConfig;
    } else {
      config += '    relabel_configs: *relabel_config\n';
    }
    config += '\n';
  }
  config += `  ### Account metrics
  - job_name: 'latency-at-account'
    bearer_token: '${token}'
    scheme: https
    metrics_path: /api/metrics
    static_configs:
      - targets:
        - api.latency.at\n`;
  return {config, targetCount};
}

export default class ConfigGenerator extends React.Component {
  constructor(props) {
    super(props);
    let enabledSatellites = {};
    for (let provider of satellites) {
      for (let region of provider[2]) {
        let id = region[0] + '.' + provider[0];
        enabledSatellites[id] = true;
      }
    }
    this.state = {
      enabledSatellites: enabledSatellites,
      http_2xx: 'https://latency.at',
      scrapeInterval: 30,
      requests: 0,
    };
    this.requestsLast = 0;
    this.toggleSatellite = this.toggleSatellite.bind(this);
  }
  componentDidMount() {
    this.props.setRefs(this.refs);
  }
  toggleSatellite(sat) {
    return () => {
      let enabledSatellites = Object.assign({}, this.state.enabledSatellites);
      enabledSatellites[sat] = !enabledSatellites[sat];
      this.setState({
        enabledSatellites: enabledSatellites,
      });
    };
  }

  render() {
    const {config, targetCount} = genConfig(this.props.token, this.state);
    const requests = targetCount*(60/this.state.scrapeInterval);
    const requestsMonth = requests*43800;

    let first = true;
    let modulesView = modules.map((module) => {
      if (first) {
       first = false;
      }
      const name = module[0];
      return <div className="module" key={name}>
          <h5>{module[0]}</h5>
          <div>{module[1]}</div>
          <TextareaAutosize
            id={module[0]}
            placeholder={module[2]}
            value={this.state[module[0]]}
            onChange={(e) => {
              this.setState({[module[0]]: e.target.value});
            }}/>
        </div>;
    });

    let providersView = satellites.map((provider) => {
      let regionView = provider[2].map((region) => {
        let id = region[0] + '.' + provider[0];
        return <div className="satellite" key={id}>
          <label>
            <input type="checkbox"
              id={id}
              checked={this.state.enabledSatellites[id]}
              onChange={this.toggleSatellite(id)}/>
            {region[1]}
          </label>
        </div>;
      });
      return <div id="satellites" key={provider[1]}>
        <h5>{provider[1]}</h5>
        {regionView}
      </div>;
    });

    let recommendedPlan;
    if (this.props.plans) {
      for (let plan of this.props.plans) {
        // has enough requests?
        let pr = parseInt(plan.metadata.requests);
        if (pr > requestsMonth) {
          // smaller plan?
          if (!recommendedPlan ||
            pr < parseInt(recommendedPlan.metadata.requests)) {
            recommendedPlan = plan;
          }
        }
      }
    }

    const countMonth = <CountTo
      from={this.requestsLast*43800}
      to={requestsMonth}
      speed={1000}>
        {(value) => <span>{value.toLocaleString()}</span>}
      </CountTo>;

    const countMin = <CountTo
      from={this.requestsLast}
      to={requests}
      speed={1000}>
        {(value) => <span>{value}/min</span>}
      </CountTo>;

    this.requestsLast = requests;

    return <div id="config-generator">
      <h4>Targets</h4>
      {modulesView}
      <h4>Scrape interval in seconds</h4>
      <div className="input-number">
        <span onClick={() =>
          this.setState({scrapeInterval: this.state.scrapeInterval + 5})
        }>+</span>
        <input
          id="scrape_interval"
          type="number"
          placeholder="Interval in seconds"
          value={this.state.scrapeInterval}
          onChange={(e) => this.setState({scrapeInterval: e.target.value})}
        />
        <span onClick={() =>
          this.setState({scrapeInterval: this.state.scrapeInterval - 5})
        }>-</span>
      </div>

      <h4 ref="satellites">Probes</h4>
      {providersView}


      <h4 ref="download">Result</h4>
      <div id="download" style={{marginBottom: '2em'}}>
        <p>
        This configuration sends about {countMonth} requests per
        month ({countMin}). You need at least the "{recommendedPlan.name}" plan
        to run this on one Prometheus server 24/7.
        </p>
        <div className="button-bar">
          <ClipboardButton
            className="button-primary"
            data-clipboard-text={config}>
            Copy
          </ClipboardButton>
          <button className="button-primary" onClick={() => {
            let blob = new Blob([config], {
              type: 'text/plain;charset=utf-8',
            });
            FileSaver.saveAs(blob, 'prometheus.yml');
          }}>
            Download
          </button>
        </div>
      </div>
      <pre>{config}</pre>
    </div>;
  }
};


ConfigGenerator.propTypes = {
  // Required but leading to false positives due to usage of cloneElement.
  plans: PropTypes.array,
  token: PropTypes.string,
  setRefs: PropTypes.func.isRequired,
};
