import React from 'react';
import PropTypes from 'prop-types';

const Plan = (props) => {
  const plan = props.plan;

  return <div className="plan-box" key={'plan-' + plan.name}>
      <span className="plan-title">{plan.name}</span>
      <div className="plan-info">
        <span className="plan-price">${plan.amount/100}/Month</span>
        <span className="plan-req">
          {Number(plan.metadata.requests).toLocaleString()} reqs
        </span>
        <span className="plan-req-min">
          ~{(plan.metadata.requests/43800).toFixed(2)} req/min
        </span>
      </div>
      {props.children}
    </div>;
};

export default Plan;
Plan.propTypes = {
  children: PropTypes.element.isRequired,
  plan: PropTypes.object.isRequired,
};
