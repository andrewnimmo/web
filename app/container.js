import React from 'react';
import PropTypes from 'prop-types';

import Alert from './components/alert';
import CookieConsent from './components/cookie_consent';

import config from 'config';

import APIClient from './client';
let client = new APIClient(config.api);

let url = new URL(window.location.href);

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.notify = this.notify.bind(this);
    this.handleAlertDismiss = this.handleAlertDismiss.bind(this);
    this.state = {};
  }
  notify(message, status) {
    console.log(message, status);
    if (message instanceof Error) {
      message = message.message;
      if (!status) {
        status = 'danger';
      };
    }
    if (this.state.notification) {
      this.setState({notification: undefined});
    }
    this.setState({notification: {status: status, message: message}});
    setTimeout(() => {
      let n = this.state.notification;
      n.close = true;
      this.setState({notification: n});
    }, 3000);
  }
  handleAlertDismiss(e) {
    this.setState({notification: null});
  }
  render() {
    let childs = React.Children.map(this.props.children, (child) => {
      return React.cloneElement(child, {
        api: client,
        notify: this.notify,
      });
    });

    let title =
      'Performance and Availability Metrics For Your Monitoring System.';
    if (this.props.location.pathname != '/') {
      title = this.props.location.pathname.charAt(1).toUpperCase()
        + this.props.location.pathname.substring(2);
    }
    document.title = 'Latency.at: ' + title;

    let notifications;
    if (this.state.notification) {
      let style = {
        animationName: this.state.notification.close ? 'slideout' : 'slidein',
      };
      if (this.state.notification.close) {
        style.left = '-100%';
      } else {
        style.left = '0';
      };

      notifications = this.state.notification &&
        <Alert slidein
          style={style}
          status={this.state.notification.status}
          onDismiss={this.handleAlertDismiss}>
          <span>{this.state.notification.message}</span>
        </Alert>;
    };

    return <div>
      <ul className={ url.searchParams.get('debug') ? 'debug' : 'hidden' }>
        <li>Env: {config.env}</li>
        <li>API: {config.api}</li>
        <li>Revision: {__GIT_SHA__}</li>
        <li>Branch: {__GIT_BRANCH__}</li>
        <li>Tag: {__GIT_TAG__}</li>
      </ul>
      { config.prerender || <CookieConsent /> }
      { notifications }
      { childs }
    </div>;
  }
};

App.propTypes = {
  children: PropTypes.element.isRequired,
  location: PropTypes.object.isRequired,
};
